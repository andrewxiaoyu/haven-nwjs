/**
 * Created by andrew on 1/3/17.
 */
$(document).ready(function(){
    firebaseSetup();
    $(".haven").hide();
    $(".flipsterhaven").hide();
    $(".loader").hide();
    $(".login").show();

    setup();
});

//General Functions
function setup(){
    $("#winclose").click(closeWindow);
    setUpQuote();
}
function closeWindow(){
    var win = nw.Window.get();
    win.close();
}