/**
 * Created by Andrew Wang on 8/9/2017.
 */

var task = function(date, name) {
    var str = '<li><span class="glyphicon glyphicon-ok"></span><p>';
    str += date + ' Completed ' + '<span class="loghighlight">';
    str += name + '</span>';
    str += '</p></li>';
    return str;
};
var begin = function(date) {
    var str = '<li><span class="glyphicon glyphicon-home"></span><p>';
    str += date + ' Project created!';
    str += '</p></li>';
    return str;
};
var finish = function(date) {
    var str = '<li><span class="glyphicon glyphicon-folder-close"></span><p>';
    str += date + ' Project closed.';
    str += '</p></li>';
    return str;
};
var tag = function(date, before, after) {
    var str = '<li><span class="glyphicon glyphicon-tag"></span><p>';
    str += date + ' Tag from <span class="loghighlight">';
    str += before + '</span> to <span class="loghighlight">' +after;
    str += '</span></p></li>';
    return str;
};
var tagline = function(date, before, after) {
    var str = '<li><span class="glyphicon glyphicon-tags"></span><p>';
    str += date + ' Tagline from <span class="loghighlight">';
    str += before + '</span> to <span class="loghighlight">' +after;
    str += '</span></p></li>';
    return str;
};
var namelog = function(date, before, after) {
    var str = '<li><span class="glyphicon glyphicon glyphicon-font"></span><p>';
    str += date + ' Name from <span class="loghighlight">';
    str += before + '</span> to <span class="loghighlight">' +after;
    str += '</span></p></li>';
    return str;
};

var methodDict = {
    "task":task,
    "start":begin,
    "end":finish,
    "tagline":tagline,
    "name":namelog,
    "tag":tag
};
function addLog(type,date, one, two){
    $("#project-log").prepend($(methodDict[type](date, one, two)));
}