/**
 * Created by Andrew Wang on 8/8/2017.
 */

//Database Access
var uid;
var database;

var myflipster;
var tagDict={};
var backup;
var data;

$(document).ready(function(){
    nw.Window.get().maximize();
    backup = $(".flipster").clone();
    setupQuote();
    accountSetup();
});

function setupQuote(){
    $.ajax({
        type: "GET",
        url: "http://api.forismatic.com/api/1.0/",
        data: {
            method:"getQuote",
            format:"json",
            lang:"en"
        },
        success: function(result){
            $(".quote").text(result.quoteText);
            $(".blockquote-footer").text(result.quoteAuthor);
        }
    });
}

function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            addPanels();
        } else {
            window.location="index.html";
        }
    });
}
function addPanels(){
    var ref = database.ref("users/" + uid + "/project/");
    ref.once('value').then(function (snapshot) {
        if(snapshot.val()!=null) {
            data = snapshot.val();
            $.each(snapshot.val(), function (k, v) {

                var color = idealTextColor(v.color);
                var str = '<li class="'+v.tag+'" style="background-color:' + v.color + '; color:'+ color +'">';
                str += '<div class = "boxoutside">';
                str += '<h2 class="project-name" onclick="startProject(\'' + k + '\')">' + v.name + '</h2>';
                str += '<h3 class="project-tagline">' + v.tagline + '</h3>';
                str += '<div class="tag-list">';
                str += '<p class="tag">#' + v.tag + '</p>';
                str += '</div></div></li>';
                $('#flipster-list').append($(str));

                var t = v.tag.replace(/ /g, '+');
                if(!tagDict[t]){
                    tagDict[t]=true;
                    var s = '<p class="flipster-tag" tag="'+t+'">#'+t+'</p>';
                    $("#flipster-tags").append($(s));
                }
            });
        }
        setupTags();
        setupFlipster();
        console.log(data);
    });
}
function setupTags(){
    $(".flipster-tag").click(function(){
        var t = $(this).attr("tag");
        console.log(t);

        if($(this).hasClass("unselected")){
            $(this).removeClass("unselected");
            $("li").show();
            t = "";
        }
        else{
            $(".flipster-tag").removeClass("unselected");
            $(this).addClass("unselected");
            $("li").hide();
            $("."+t).show();
        }
        rerender(t);
    });
}
function setupFlipster(){
    myflipster = $('.flipster').flipster({
        start:0,
        style: 'carousel'
    });
    $(".loader").hide();
    $(".flipsterhaven").show();
}
function rerender(tag){
    $(".flipsterhaven").hide();
    $(".loader").show();
    var flp = $(".flipster-outside");
    flp.empty();
    flp.append(backup.clone());
    console.log(tag);
    $.each(data, function (k, v) {
        var t = v.tag.replace(/ /g, '+');
        console.log(t);
        if(tag=="" || tag==t){
            var color = idealTextColor(v.color);
            var str = '<li class="'+v.tag+'" style="background-color:' + v.color + '; color:'+ color +'">';
            str += '<div class = "boxoutside">';
            str += '<h2 class="project-name" onclick="startProject(\'' + k + '\')">' + v.name + '</h2>';
            str += '<h3 class="project-tagline">' + v.tagline + '</h3>';
            str += '<div class="tag-list">';
            str += '<p class="tag">#' + v.tag + '</p>';
            str += '</div></div></li>';
            $('#flipster-list').append($(str));
        }
    });
    setupFlipster();
}

function addProject(){
    window.location="add.html";
}
function startProject(project){
    sessionStorage.setItem("project",project);
    window.location = "project.html";
}

function signOut(){
    firebase.auth().signOut().then(function() {
        localStorage.setItem('email', null);
        localStorage.setItem('password', null);
        window.location = "index.html";
    }).catch(function(error) {
        console.log(error);
    });
}
function stats(){
    window.location = "stats.html";
}
function archive(){
    window.location = "archive.html";
}