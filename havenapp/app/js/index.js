/**
 * Created by Andrew Wang on 8/8/2017.
 */

//Node webkit's native UI library
var gui = require('nw.gui');

//Database Access
var database;

$(document).ready(function(){
    automatic();
});
function automatic(){
    var email = localStorage.getItem('email');
    var pass = localStorage.getItem('password');
    if(email!="null" && email!=null && pass !="null" && pass!=null){
        console.log(email);
        firebase.auth().signInWithEmailAndPassword(email, pass).then(function() {
            proceed();
        });
    }
}
function proceed(){
    window.location="select.html";
}

//Firebase Methods
function auth() {
    $(".login").hide();
    $(".loader").show();

    firebase.auth().signInWithEmailAndPassword($("#email").val(), $("#password").val()).then(function() {
        localStorage.setItem('email', $("#email").val());
        localStorage.setItem('password', $("#password").val());
        proceed();
    }, function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        $(".loader").hide();
        $(".login").show();
        switch(errorCode) {
            case "auth/invalid-email":
                $("#loginerrors").text("Invalid Email.");
                break;
            case "auth/user-disabled":
                $("#loginerrors").text("User has been disabled. Contact us!");
                break;
            case "auth/user-not-found":
                $("#loginerrors").text("User not found.");
                break;
            case "auth/wrong-password":
                $("#loginerrors").text("Wrong password. Try Again!");
                break;
            default:
                $("#loginerrors").text("Unable to log you in.");
        }
    });
}
function register() {
    $(".login").hide();
    $(".loader").show();

    firebase.auth().createUserWithEmailAndPassword($("#email").val(), $("#password").val()).then(function() {
        localStorage.setItem('email', $("#email").val());
        localStorage.setItem('password', $("#password").val());
        var uid = firebase.auth().currentUser.uid;
        var database = firebase.database();
        database.ref("users/" + uid + "/stats/").set({
            notesCount : 0,
            linksCount : 0,
            projectsCount : 0,
            archiveCount : 0,
            tasksCount : 0,
            tasksTotal : 0
        }, function(){
            proceed();
        });
    }, function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        $(".loader").hide();
        $(".login").show();

        switch(errorCode) {
            case "auth/invalid-email":
                $("#loginerrors").text("Invalid Email.");
                break;
            case "auth/email-already-in-use":
                $("#loginerrors").text("Email is in use. Try logging in!");
                break;
            case "auth/operation-not-allowed":
                $("#loginerrors").text("Unable to create account.");
                break;
            case "auth/weak-password":
                $("#loginerrors").text("Weak password. Try making it longer!");
                break;
            default:
                $("#loginerrors").text("Unable to create account.");
        }
    });
}