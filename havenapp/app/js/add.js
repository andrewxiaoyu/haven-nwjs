/**
 * Created by Andrew Wang on 8/9/2017.
 */

//Database Access
var uid;
var database;

var project;

$(document).ready(function(){
    accountSetup();
});

function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            if(sessionStorage.getItem("project")!=null && sessionStorage.getItem("project")!="null"){
                setupPage();
            }
        } else {
            window.location="index.html";
        }
    });
}
function setupPage(){
    project = sessionStorage.getItem("project");
    console.log(project);
    var ref = database.ref("users/" + uid + "/project/" + project);
    ref.once('value').then(function (snapshot) {
        var obj = snapshot.val();
        $("#name").val(obj.name);
        $("#tagline").val(obj.tagline);
        $("#tag").val(obj.tag);
        $("#color").val(obj.color);

        $(".title").text("Edit Project");
        $("#submit").text("Edit");
        console.log($("#submit"));
    });
}


function createProject(){
    var moment = require('moment');
    if(project==null){
        var ref = firebase.database().ref('users/' + uid + '/project/');
        var newPostRef = ref.push();
        if($("#tagline").val().replace(/ /g, '') != ""){
            newPostRef.set({
                name: $("#name").val(),
                tagline: $("#tagline").val(),
                tag: $("#tag").val().replace(/ /g, '+'),
                color: $("#color").val(),
                start: new Date(),
                log:{
                    0:{
                        type: "start",
                        date: moment().format('M/D/YY')
                    }
                }
            });
            var ref2 = firebase.database().ref('users/' + uid + '/stats/projectsCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() + 1;
                firebase.database().ref('users/' + uid + '/stats/projectsCount').set(obj);
                window.location="select.html";
            });
        }
        else{
            $("#adderrors").text("Please enter a tagline!");
        }
    }
    else{
        var ref = database.ref("users/" + uid + "/project/" + project);
        ref.once('value').then(function (snapshot) {
            var obj = snapshot.val();
            if($("#name").val() != obj.name){
                database.ref("users/" + uid + "/project/" + project + "/name").set($("#name").val());
                var newPostRef = database.ref("users/" + uid + "/project/" + project + "/log/").push();
                newPostRef.set({
                    type: "name",
                    date: moment().format('M/D/YY'),
                    val: obj.name,
                    val2: $("#name").val()
                });
            }
            if($("#tagline").val() != obj.tagline){
                database.ref("users/" + uid + "/project/" + project + "/tagline").set($("#tagline").val());
                var newPostRef = database.ref("users/" + uid + "/project/" + project + "/log/").push();
                newPostRef.set({
                    type: "tagline",
                    date: moment().format('M/D/YY'),
                    val: obj.tagline,
                    val2: $("#tagline").val()
                });
            }
            if($("#tag").val() != obj.tag){
                database.ref("users/" + uid + "/project/" + project + "/tag").set($("#tag").val());
                var newPostRef = database.ref("users/" + uid + "/project/" + project + "/log/").push();
                newPostRef.set({
                    type: "tag",
                    date: moment().format('M/D/YY'),
                    val: obj.tag,
                    val2: $("#tag").val().replace(/ /g, '+')
                });
            }
            if($("#color").val() != obj.color){
                database.ref("users/" + uid + "/project/" + project + "/color").set($("#color").val());
            }

            window.location="project.html";
        });
    }
}