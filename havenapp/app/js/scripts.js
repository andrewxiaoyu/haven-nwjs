/**
 * Created by Andrew Wang on 8/8/2017.
 */

var config = {
    apiKey: "AIzaSyC8MTogEcc-n00ZoMwNCPualaoc0_wHRSE",
    authDomain: "haven-4235b.firebaseapp.com",
    databaseURL: "https://haven-4235b.firebaseio.com",
    projectId: "haven-4235b",
    storageBucket: "",
    messagingSenderId: "41538683484"
};
firebase.initializeApp(config);

$(document).ready(function(){
    setup();
});

//General Functions
function setup(){
    $("#winclose").click(function(){nw.Window.get().close()});
}
function idealTextColor(bgColor) {
    var nThreshold = 105;
    var components = getRGBComponents(bgColor);
    var bgDelta = (components.R * 0.299) + (components.G * 0.587) + (components.B * 0.114);

    return ((255 - bgDelta) < nThreshold) ? "#000000" : "#ffffff";
}

function getRGBComponents(color) {

    var r = color.substring(1, 3);
    var g = color.substring(3, 5);
    var b = color.substring(5, 7);

    return {
        R: parseInt(r, 16),
        G: parseInt(g, 16),
        B: parseInt(b, 16)
    };
}
