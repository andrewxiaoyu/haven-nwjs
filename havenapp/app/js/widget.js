/**
 * Created by Andrew Wang on 8/9/2017.
 */

//Database Access
var uid;
var database;
var project;

var task = {};

var moment;
var mode = "tasks";

$(document).ready(function(){
    initialize();
    closeTask();
    accountSetup();
});

function closeTask(){
    nw.Window.get().on('close', function() {
        finishTasks();
        if (nw.Window.get() != null)
            nw.Window.get().close(true);
    });
}
function finishTasks(){
    for (var id in task) {
        clearTimeout(task[id]);
        if(task[id]!=null){
            task[id] = null;

            var par = $("li[key=\'"+id+"\']");
            console.log(par);
            var newPostRef = database.ref("users/" + uid + "/project/" + project +"/log/").push();
            newPostRef.set({
                type: "task",
                date: moment().format('M/D/YY'),
                val: $(par.find("p")).text()
            });
            par.fadeOut("fast");
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
            var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() + 1;
                firebase.database().ref('users/' + uid + '/stats/tasksCount').set(obj);
            });
        }
    }
}

function initialize(){
    var win = nw.Window.get();
    nw.Screen.Init();
    var screeninfo = nw.Screen.screens[0].work_area;
    moment = require('moment');
    win.setAlwaysOnTop(true);
    win.setPosition("center");
    win.setResizable(true);
    win.setMinimumSize(100, 100);
    win.resizeTo(400,200);
    win.moveTo(screeninfo.width-400, screeninfo.height-200);
    console.log(screeninfo.width);
    console.log(screeninfo.height);
}
function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            setupProject();
        } else {
            window.location="index.html";
        }
    });
}
function setupProject(){
    project = sessionStorage.getItem("widget");
    sessionStorage.setItem("widget", null);
    var ref = database.ref("users/" + uid + "/project/" + project);
    ref.on('value', function(snapshot){
        var obj = snapshot.val();
        $("#toptitle").text(obj.name);
        $("#project-notes").empty();
        $("#project-tasks").empty();

        $("#project-notes").hide();
        $("#project-tasks").show();

        if(obj.tasks != null) {
            $.each(obj.tasks, function (k, v) {
                addTask(k, v);
            });
        }
        if(obj.notes != null) {
            $.each(obj.notes, function (k, v) {
                addNote(k, v);
            });
        }
        setupWidget();
        setupTasks();
        setupNotes();
    });
    $("#toplogo").click(function(){
        finishTasks();
        sessionStorage.setItem("project", project);
        var win = nw.Window.get();
        win.setAlwaysOnTop(false);
        win.maximize();
        nw.Window.get().maximize();
        window.location = "project.html";
    });
}

function setupWidget(){
    console.log("Hello");
    $('#inputbar').off();
    if(mode == "notes"){
        console.log("notes");
        $(this).attr("placeholder","Something to remember");
        $("#project-tasks").hide();
        $("#project-notes").show();
        $('#inputbar').keydown(function (e){
            if(e.keyCode == 13){
                var val = $(this).val();
                if(val.replace(/ /g, '') != "") {
                    var ref = firebase.database().ref('users/' + uid + '/project/' + project + "/notes");
                    var newPostRef = ref.push();
                    newPostRef.set(val);
                    var ref2 = firebase.database().ref('users/' + uid + '/stats/notesCount');
                    ref2.once('value').then(function (snapshot) {
                        var obj = snapshot.val() + 1;
                        firebase.database().ref('users/' + uid + '/stats/notesCount').set(obj);
                    });
                    $(this).val("");
                }
            }
            if (e.keyCode == 9) {
                console.log("TABBED");
                e.preventDefault();
                if(mode == "tasks"){
                    mode = "notes";
                }
                else if(mode == "notes"){
                    mode = "tasks";
                }
                setupWidget();
            }
        });
    }
    else if(mode == "tasks"){
        console.log("tasks");
        $(this).attr("placeholder","New task");
        $("#project-notes").hide();
        $("#project-tasks").show();
        $('#inputbar').keydown(function (e){
            if(e.keyCode == 13){
                var val = $(this).val();
                if(val.replace(/ /g, '') != "") {
                    var ref = firebase.database().ref('users/' + uid + '/project/' + project + "/tasks");
                    var newPostRef = ref.push();
                    newPostRef.set(val);
                    $(this).val("");
                    var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksTotal');
                    ref2.once('value').then(function (snapshot) {
                        var obj = snapshot.val() + 1;
                        firebase.database().ref('users/' + uid + '/stats/tasksTotal').set(obj);
                    });
                }
            }
            if (e.keyCode == 9) {
                console.log("TABBED");
                e.preventDefault();
                if(mode == "tasks"){
                    mode = "notes";
                }
                else if(mode == "notes"){
                    mode = "tasks";
                }
                setupWidget();
            }
        });
    }
}
function setupTasks(){
    $("#project-tasks .glyphicon-unchecked").click(function(){
        var par = $($(this).parent());
        par.addClass("checked");
        $(this).hide();
        $(par.find(".glyphicon-check")[0]).show();
        var id = par.attr("key");
        task[id] = setTimeout(function(){
            task[id] = null;
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
            var newPostRef = database.ref("users/" + uid + "/project/" + project +"/log/").push();
            newPostRef.set({
                type: "task",
                date: moment().format('M/D/YY'),
                val: $(par.find("p")).text()
            });
            par.fadeOut("fast");
            var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() + 1;
                firebase.database().ref('users/' + uid + '/stats/tasksCount').set(obj);
            });
        }, 3000);
    });
    $("#project-tasks .glyphicon-check").click(function(){
        var par = $($(this).parent());
        par.removeClass("checked");
        $(this).hide();
        $(par.find(".glyphicon-unchecked")[0]).show();
        var id = par.attr("key");
        clearTimeout(task[id]);
    });
    $("#project-tasks p").keydown(function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            var text = $(this).text();
            var id = $($(this).parent()).attr("key");
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(text);
        }
    });
    $("#project-tasks .glyphicon-remove-sign").click(function(){
        var id = $($(this).parent()).attr("key");
        database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
        $($(this).parent()).hide();
    });
}
function setupNotes(){
    $("#project-notes p").keydown(function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            var text = $(this).text();
            var id = $($(this).parent()).attr("key");
            console.log(text);
            database.ref("users/" + uid + "/project/" + project +"/notes/" + id).set(text);
        }
    });
    $("#project-notes .glyphicon-remove-sign").click(function(){
        var id = $($(this).parent()).attr("key");
        database.ref("users/" + uid + "/project/" + project +"/notes/" + id).set(null);
        $($(this).parent()).hide();
    });
}

function addTask(key, text){
    var str = "";
    if(key in task){
        str += '<li key="'+key+'" class="checked">';
        str += '<span class="glyphicon glyphicon-unchecked" style="display:none"></span>';
        str += '<span class="glyphicon glyphicon-check" style="display:inline-block"></span>';
    }
    else{
        str += '<li key="'+key+'">';
        str += '<span class="glyphicon glyphicon-unchecked"></span>';
        str += '<span class="glyphicon glyphicon-check" style="display: none"></span>';
    }
    str += '<p contenteditable="true">';
    str+=text;
    str+='</p><span class="glyphicon glyphicon-remove-sign"></span></li>';
    $("#project-tasks").append($(str));
}
function addNote(key, text){
    var str = '<li key="'+key+'"><p contenteditable="true">';
    str += text;
    str += '</p><span class="glyphicon glyphicon-remove-sign"></span></li>';
    $("#project-notes").append($(str));
}
