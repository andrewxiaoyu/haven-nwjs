/**
 * Created by Andrew Wang on 8/15/2017.
 */

/**
 * Created by Andrew Wang on 8/8/2017.
 */

//Database Access
var uid;
var database;

var options = {
    legend: {
        display: false
    },
    tooltips: {
        bodyFontColor: "#FFF",
        titleFontColor: "#FFF"
    }
}

$(document).ready(function(){
    accountSetup();
});
function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            addPanels();
        } else {
            window.location="index.html";
        }
    });
}
function addPanels(){
    var ref = database.ref("users/" + uid + "/stats");
    ref.once('value').then(function (snapshot) {
        var obj = snapshot.val();
        if(obj != null) {
            var otherc = $("#otherc");
            var projectsc = $("#projectsc");
            var tasksc = $("#tasksc");

            var ot = new Chart(otherc, {
                type: 'bar',
                data: {
                    labels: ["Links", "Notes", "Tasks",  "Projects"],
                    datasets: [{
                        label: '# of Votes',
                        data: [obj.linksCount, obj.notesCount, obj.tasksTotal, obj.projectsCount]
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        displayColors: false,
                        bodyFontColor: "#FFF",
                        titleFontColor: "#FFF"
                    }
                }
            });

            var pr = new Chart(projectsc, {
                type: 'doughnut',
                data: {
                    labels: ["Completed", "Open"],
                    datasets: [{
                        data: [obj.archiveCount, obj.projectsCount - obj.archiveCount]
                    }]
                },
                options: options
            });

            var ta = new Chart(tasksc, {
                type: 'doughnut',
                data: {
                    labels: ["Completed", "Open"],
                    datasets: [{
                        data: [obj.tasksCount, obj.tasksTotal - obj.tasksCount]
                    }]
                },
                options: options
            });
        }
        setupFlipster();
    });
}
function setupFlipster(){
    $('.flipster').flipster({
        style: 'carousel'
    });
    $(".loader").hide();
    $(".flipsterhaven").show();
}

function returnHome(){
    window.location="select.html";
}