/**
 * Created by Andrew Wang on 8/15/2017.
 */

/**
 * Created by Andrew Wang on 8/8/2017.
 */

//Database Access
var uid;
var database;

var project;

$(document).ready(function(){
    accountSetup();
});
function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            addPanels();
        } else {
            window.location="index.html";
        }
    });
}
function addPanels(){
    var ref = database.ref("users/" + uid + "/archive/");
    ref.once('value').then(function (snapshot) {
        if(snapshot.val()!=null) {
            $.each(snapshot.val(), function (k, v) {
                var color = idealTextColor(v.color);
                var str = '<li style="background-color:' + v.color + '; color:'+ color +'">';
                str += '<div class = "project"><h2 class="project-name">' + v.name + '</h2>';
                str += '<h3 class="project-tagline">' + v.tagline + '</h3>';
                str += '<button type="submit" class="btn project-restore" key="' + k + '">Restore</button>';
                str += '</div></li>';
                $('#flipster-list').append($(str));
            });
        }
        setupFlipster();
    });
}
function setupFlipster(){
    $('.flipster').flipster({
        style: 'carousel'
    });
    $(".project-restore").click(function () {
        var ref = database.ref("users/" + uid);
        project = $(this).attr("key");
        ref.once('value').then(function (snapshot) {
            var obj = snapshot.val();
            firebase.database().ref('users/' + uid + '/project/'+project).set(obj.archive[project]);
            firebase.database().ref('users/' + uid + '/archive/'+project).remove();

            sessionStorage.setItem("project", project);

            var ref2 = firebase.database().ref('users/' + uid + '/stats/archiveCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() - 1;
                firebase.database().ref('users/' + uid + '/stats/archiveCount').set(obj);
                window.location="project.html";
            });
        });
    });
    $(".loader").hide();
    $(".flipsterhaven").show();
}

function returnHome(){
    window.location="select.html";
}