/**
 * Created by Andrew Wang on 8/9/2017.
 */

//Database Access
var uid;
var database;
var project;

var task = {};
var closeFinish = 0;

var moment;

$(document).ready(function(){
    accountSetup();
    closeTask();
    moment = require('moment');
});

function closeTask(){
    nw.Window.get().on('close', function() {
        finishTasks();
        if (nw.Window.get() != null){
            nw.Window.get().close(true);
        }
    });
}
function finishTasks(){
    console.log(task);
    for (var id in task) {
        clearTimeout(task[id]);
        console.log(task[id]);
        if(task[id]!=null){
            task[id] = null;

            var par = $("li[key=\'"+id+"\']");
            console.log(par);
            var newPostRef = database.ref("users/" + uid + "/project/" + project +"/log/").push();
            newPostRef.set({
                type: "task",
                date: moment().format('M/D/YY'),
                val: $(par.find("p")).text()
            });
            par.fadeOut("fast");
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
            var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() + 1;
                firebase.database().ref('users/' + uid + '/stats/tasksCount').set(obj);
            });
        }
    }
}

function accountSetup(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = user.uid;
            database = firebase.database();
            setupProject();
        } else {
            window.location="index.html";
        }
    });
}
function setupProject(){
    project = sessionStorage.getItem("project");
    sessionStorage.setItem("project", null);
    if(project==null || project == "null"){
        window.location = "select.html";
    }
    var ref = database.ref("users/" + uid + "/project/" + project);
    ref.on('value', function(snapshot) {
        var obj = snapshot.val();
        $("#project-notes").empty();
        $("#project-tasks").empty();
        $("#project-log").empty();
        $("#project-links").empty();

        $("#projecttitle").text(obj.name);
        $("#projecttagline").text(obj.tagline);

        if(obj.log != null){
            $.each(obj.log, function (k, v) {
                addLog(v.type, v.date, v.val, v.val2);
            });
        }
        if(obj.tasks != null) {
            $.each(obj.tasks, function (k, v) {
                addTask(k, v);
            });
        }
        if(obj.notes != null) {
            $.each(obj.notes, function (k, v) {
                addNote(k, v);
            });
        }
        if(obj.links != null) {
            $.each(obj.links, function (k, v) {
                addLink(k, v);
            });
        }

        setupFields();
        setupTasks();
        setupNotes();
        setupLinks();
        $(".boxcontainer").hide();
        $("#project-container").show();
    });
}

function setupFields(){
    $('#projecttitle').keydown(function (e){
        if(e.keyCode == 13){
            e.preventDefault();
            var ref = database.ref("users/" + uid + "/project/" + project);
            ref.once('value').then(function (snapshot) {
                var obj = snapshot.val();
                var text = $("#projecttitle").text();

                if(text.replace(/ /g, '') != "") {
                    database.ref("users/" + uid + "/project/" + project + "/name").set(text);
                    var newPostRef = database.ref("users/" + uid + "/project/" + project + "/log/").push();
                    newPostRef.set({
                        type: "name",
                        date: moment().format('M/D/YY'),
                        val: obj.name,
                        val2: text
                    });
                }
                else{
                    $("#projecttitle").text(obj.name);
                }
            });
        }
    });
    $('#projecttagline').keydown(function (e){
        if(e.keyCode == 13) {
            e.preventDefault();
            var ref = database.ref("users/" + uid + "/project/" + project);
            ref.once('value').then(function (snapshot) {
                var obj = snapshot.val();
                var text = $("#projecttagline").text();
                if(text.replace(/ /g, '') != "") {
                    database.ref("users/" + uid + "/project/" + project + "/tagline").set(text);
                    var newPostRef = database.ref("users/" + uid + "/project/" + project + "/log/").push();
                    newPostRef.set({
                        type: "tagline",
                        date: moment().format('M/D/YY'),
                        val: obj.tagline,
                        val2: text
                    });
                }
                else{
                    $("#projecttagline").text(obj.tagline);
                }
            });
        }
    });
}
function setupTasks(){
    $('#task').keydown(function (e){
        if(e.keyCode == 13){
            var val = $(this).val();
            if(val.replace(/ /g, '') != "") {
                var ref = firebase.database().ref('users/' + uid + '/project/' + project + "/tasks");
                var newPostRef = ref.push();
                newPostRef.set(val);
                $(this).val("");
                var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksTotal');
                ref2.once('value').then(function (snapshot) {
                    var obj = snapshot.val() + 1;
                    firebase.database().ref('users/' + uid + '/stats/tasksTotal').set(obj);
                });
            }
        }
    });
    $("#project-tasks .glyphicon-unchecked").click(function(){
        var par = $($(this).parent());
        par.addClass("checked");
        $(this).hide();
        $(par.find(".glyphicon-check")[0]).show();
        var id = par.attr("key");
        task[id] = setTimeout(function(){
            task[id]=null;
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
            var newPostRef = database.ref("users/" + uid + "/project/" + project +"/log/").push();
            newPostRef.set({
                type: "task",
                date: moment().format('M/D/YY'),
                val: $(par.find("p")).text()
            });
            par.fadeOut("fast");
            var ref2 = firebase.database().ref('users/' + uid + '/stats/tasksCount');
            ref2.once('value').then(function (snapshot) {
                var obj = snapshot.val() + 1;
                firebase.database().ref('users/' + uid + '/stats/tasksCount').set(obj);
            });
        }, 2000);
    });
    $("#project-tasks .glyphicon-check").click(function(){
        var par = $($(this).parent());
        par.removeClass("checked");
        $(this).hide();
        $(par.find(".glyphicon-unchecked")[0]).show();
        var id = par.attr("key");
        clearTimeout(task[id]);
        task[id] = null;
    });
    $("#project-tasks p").keydown(function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            var text = $(this).text();
            var id = $($(this).parent()).attr("key");
            database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(text);
        }
    });
    $("#project-tasks .glyphicon-remove-sign").click(function(){
        var id = $($(this).parent()).attr("key");
        database.ref("users/" + uid + "/project/" + project +"/tasks/" + id).set(null);
        $($(this).parent()).hide();
    });
}
function setupNotes(){
    $('#note').keydown(function (e){
        if(e.keyCode == 13){
            var val = $(this).val();
            if(val.replace(/ /g, '') != "") {
                var ref = firebase.database().ref('users/' + uid + '/project/' + project + "/notes");
                var newPostRef = ref.push();
                newPostRef.set(val);
                var ref2 = firebase.database().ref('users/' + uid + '/stats/notesCount');
                ref2.once('value').then(function (snapshot) {
                    var obj = snapshot.val() + 1;
                    firebase.database().ref('users/' + uid + '/stats/notesCount').set(obj);
                });
                $(this).val("");
            }
        }
    });
    $("#project-notes p").keydown(function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            var text = $(this).text();
            var id = $($(this).parent()).attr("key");
            database.ref("users/" + uid + "/project/" + project +"/notes/" + id).set(text);
        }
    });
    $("#project-notes .glyphicon-remove-sign").click(function(){
        var id = $($(this).parent()).attr("key");
        database.ref("users/" + uid + "/project/" + project +"/notes/" + id).set(null);
        $($(this).parent()).hide();
    });
}
function setupLinks(){
    $('#link').keydown(function (e){
        if(e.keyCode == 13){
            var val = $(this).val();
            if(val.replace(/ /g, '') != "") {
                var ref = firebase.database().ref('users/' + uid + '/project/' + project + "/links");
                var newPostRef = ref.push();
                newPostRef.set(val);
                $(this).val("");
                var ref2 = firebase.database().ref('users/' + uid + '/stats/linksCount');
                ref2.once('value').then(function (snapshot) {
                    var obj = snapshot.val() + 1;
                    firebase.database().ref('users/' + uid + '/stats/linksCount').set(obj);
                });
            }
        }
    });
    $("#project-links .glyphicon-remove-sign").click(function(){
        var id = $($(this).parent()).attr("key");
        database.ref("users/" + uid + "/project/" + project +"/links/" + id).set(null);
        $($(this).parent()).hide();
    });
    $('a[target=_blank]').on('click', function(){
        nw.Shell.openExternal( this.href );
        return false;
    });
}

function addTask(key, text){
    var str = "";
    if(key in task){
        str += '<li key="'+key+'" class="checked">';
        str += '<span class="glyphicon glyphicon-unchecked" style="display:none"></span>';
        str += '<span class="glyphicon glyphicon-check" style="display:inline-block"></span>';
    }
    else{
        str += '<li key="'+key+'">';
        str += '<span class="glyphicon glyphicon-unchecked"></span>';
        str += '<span class="glyphicon glyphicon-check" style="display: none"></span>';
    }
    str += '<p contenteditable="true">';
    str+=text;
    str+='</p><span class="glyphicon glyphicon-remove-sign"></span></li>';
    $("#project-tasks").append($(str));
}
function addNote(key, text){
    var str = '<li key="'+key+'"><p contenteditable="true">';
    str += text;
    str += '</p><span class="glyphicon glyphicon-remove-sign"></span></li>';
    $("#project-notes").append($(str));
}
function addLink(key, text){
    var str = '<li key="'+key+'"><span class="glyphicon glyphicon-link"></span><a target="_blank" href="';
    str += text;
    str += '"><p class="task">';
    str += text;
    str += '</p></a><span class="glyphicon glyphicon-remove-sign"></span></li>';
    $("#project-links").append($(str));
}

function changePage(name, val){
    console.log(name);
    switch(name) {
        case "Dashboard":
            $(".project-page").removeClass("selected");
            $(val).addClass("selected");
            $(".project-field").css("width","40vw");
            $(".project-field").show();
            break;
        case "Tasks":
            $(".project-page").removeClass("selected");
            $(val).addClass("selected");
            $(".project-field").css("width","80vw");
            $(".project-field").hide();
            $($("#project-tasks").parent()).show();
            break;
        case "Notes":
            $(".project-page").removeClass("selected");
            $(val).addClass("selected");
            $(".project-field").css("width","80vw");
            $(".project-field").hide();
            $($("#project-notes").parent()).show();
            break;
        case "Log":
            $(".project-page").removeClass("selected");
            $(val).addClass("selected");
            $(".project-field").css("width","80vw");
            $(".project-field").hide();
            $($("#project-log").parent()).show();
            break;
        case "Links":
            $(".project-page").removeClass("selected");
            $(val).addClass("selected");
            $(".project-field").css("width","80vw");
            $(".project-field").hide();
            $($("#project-links").parent()).show();
            break;
        default:
            console.log("Hello");
    }
}

function archiveProject(){
    var ref = database.ref("users/" + uid);
    ref.once('value').then(function (snapshot) {
        var obj = snapshot.val();
        firebase.database().ref('users/' + uid + '/archive/'+project).set(obj.project[project]);
        firebase.database().ref('users/' + uid + '/project/'+project).remove();

        var ref2 = firebase.database().ref('users/' + uid + '/stats/archiveCount');
        ref2.once('value').then(function (snapshot) {
            var obj = snapshot.val() + 1;
            firebase.database().ref('users/' + uid + '/stats/archiveCount').set(obj);
            window.location="select.html";
        });
    });
}
function deleteProject(){
    if (confirm("Are you sure you want to delete? Your project will be gone forever.") == true) {
        firebase.database().ref('users/' + uid + '/project/'+project).remove();
        window.location="select.html";
    } else {
    }
}
function settings(){
    finishTasks();
    sessionStorage.setItem("project",project);
    window.location="add.html";
}
function returnHome(){
    finishTasks();
    window.location="select.html";
}
function widget(){
    sessionStorage.setItem("widget",project);
    window.location = "widget.html";
}
